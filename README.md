My bash aliases hosted as a web page for easy access. See [the landing page](public/index.html).

May be fetched and applied thus:
```bash
source <(curl -L germs.dev)
```
